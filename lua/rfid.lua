PORT = 1241
ADDRESS = '192.168.1.115'

uart.setup(0, 9600, 8, uart.PARITY_NONE, uart.STOPBITS_1, 0)

gpio.mode(2, gpio.OUTPUT)
gpio.write(2, gpio.LOW)

function post_data(postdata)
  local connout = net.createConnection(net.TCP)

  connout:on(
    'receive',
    function(conn, data)
      if data == '1' then
        gpio.write(2, gpio.HIGH)
      else
        gpio.write(2, gpio.LOW)
      end
      conn:close()
    end)

  connout:on(
    'connection',
    function(conn)
      conn:send(postdata)
    end)

  connout:connect(PORT, ADDRESS)
end

function post_keepalive()
  local connout = net.createConnection(net.TCP)

  connout:on(
  'connection',
  function(conn)
    conn:send(tostring(PORT), function (conn) conn:close() end)
  end)

  connout:connect(PORT, ADDRESS)
end

wait = 0

throttle_timer = tmr.create()
throttle_timer:register(1500, tmr.ALARM_SEMI, function() wait = 0 end)

keepalive_timer = tmr.create()
keepalive_timer:alarm(60000, tmr.ALARM_AUTO, post_keepalive)  -- 1 min.

uart.on(
  'data',
  '\003',
	function(data)
		if string.len(data) == 14 then
			if wait == 0 then
				local data0 = string.sub(data, 2, 5)
				local data1 = string.sub(data, 6, 9)
				local data2 = string.sub(data, 10, 13)
				local num0 = bit.bxor(tonumber(data0, 16), 51085)
				local num1 = bit.bxor(tonumber(data1, 16), 51085)
				local num2 = bit.bxor(tonumber(data2, 16), 51085)
				post_data(string.format('%04X%04X%04X', num0, num1, num2))
			end
			wait = 1
      throttle_timer:start()
		end
	end,
  0)
