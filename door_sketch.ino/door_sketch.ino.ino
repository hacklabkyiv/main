#define INA 3
#define INB 2
#define BITS_IN_CODE  26
#define RFID_TIMEOUT  1000

int prevA = 0;
int prevB = 0;

uint32_t code = 0;
uint32_t tempCode = 0;
uint32_t cnt = 0;
uint8_t bits = 0;
  
void setup() {
  Serial.begin(115200); // open serial connection to USB Serial port

  pinMode(INA, INPUT);
  pinMode(INB, INPUT);
}

void loop() 
{
    int curA = digitalRead(INA);  
    if  (( curA != prevA ) && (curA > 0))  
    {
        tempCode <<= 1; 
        tempCode |= 1;
        bits++;
        cnt = RFID_TIMEOUT;
    }
    prevA = curA;

    int curB = digitalRead(INB);  
    if  (( curB != prevB ) && (curB > 0))  
    {
        tempCode <<= 1; 
        tempCode &= ~1;
        bits++;
        cnt = RFID_TIMEOUT;
    }
    prevB = curB;

    if (cnt) 
    {
      cnt--;
    }
    else
    {
     
      if (tempCode)
      {
         tempCode >>= (bits - BITS_IN_CODE);
         code = ((tempCode >> 1) & 0xFFFFFF ) ^ 0xFFFFFF ^ 0xC78DC7;
         char buf[7];
         sprintf(buf, "%02X%02X%02X",
                (uint8_t) ~((code >> 16) & 0xFF),
                (uint8_t) ~((code >> 8) & 0xFF),
                (uint8_t) ~( code  & 0xFF)
                );
         Serial.println(buf); 
         tempCode = 0;
      }

       bits = 0;
    }
    delayMicroseconds(100);  
}
