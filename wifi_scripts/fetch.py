import urllib2
import time
import sqlite3
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

# sqlite3 db.sqlite
# sqlite> CREATE TABLE records(time INTEGER, mac TEXT, idle INTEGER, PRIMARY KEY (time, mac));

DATABASE = 'db.sqlite'
ADDRESS = 'http://192.168.1.1/stations'
CREDENTIALS = 'cred.conf'

login, password = open(CREDENTIALS).read()[:-1].split()
database = sqlite3.connect(DATABASE)
response = urllib2.urlopen(ADDRESS)
timestamp = 0
mac = ''
for line, data in enumerate(response):
    data = data[:-1]
    if line == 0:
        timestamp = int(time.mktime(time.strptime(data, '%a %b %d %H:%M:%S %Z %Y')))
    else:
        cursor = database.cursor()
        if line % 3 == 1:
            mac = data.split()[1]
            cursor.execute('SELECT * FROM records WHERE time = ? AND mac = ?', (timestamp, mac))
            entry = cursor.fetchone()
            if entry is None:
                cursor.execute('INSERT INTO records(time, mac) VALUES (?, ?)', (timestamp, mac))
                publish.single('/'.join(['hacklab', 'presence', mac]),
                               payload=str(timestamp),
                               hostname='192.168.1.115',
                               port=1883,
                               auth={'username': login, 'password': password},
                               protocol=mqtt.MQTTv311)

        elif line % 3 == 2:
            cursor.execute('UPDATE records SET idle = ? WHERE time = ? AND mac = ?', (int(data.split()[2]), timestamp, mac))
        database.commit()

