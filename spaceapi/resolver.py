#!/usr/bin/python
# -*- coding: utf-8 -*-

import yaml
import sys
import paho.mqtt.client as mqtt
import logging
import psycopg2 as psycopg
from psycopg2 import pool

CONFIG_FILE = "config.yml"
try:
    with open(CONFIG_FILE, 'r') as ymlfile:
        config = yaml.load(ymlfile, Loader=yaml.BaseLoader)
except IOError as e:
    logging.error("Config file not found!")
    logging.error("Exception: %s" % str(e))
    sys.exit(1)

logging.basicConfig(filename=config['logging']['resolver'])
logger = logging.getLogger("resolver")

mqtt_client = mqtt.Client()

try:
    conn_pool = pool.SimpleConnectionPool(1, 20, user = config['db']['user'],
                                  password = config['db']['password'],
                                  host = config['db']['host'],
                                  port = config['db']['port'],
                                  database = config['db']['name'])
except (Exception, psycopg.DatabaseError) as error :
    logger.warning("Error while connecting to PostgreSQL", error)

def on_mqtt_connect(client, userdata, flags, rc):
    if rc == 0:
        logger.info("connected")
    elif rc == 1:
        logger.warning("Connection refused - incorrect protocol version")
    elif rc == 2:
        logger.warning("Connection refused - invalid client identifier")
    elif rc == 3:
        logger.warning("Connection refused - server unavailable")
    elif rc == 4:
        logger.warning("Connection refused - bad username or password")
    elif rc == 5:
        logger.warning("Connection refused - not authorised")
    else:
        logger.warning("Unknown connection error: %d", rc)
    client.subscribe(config['mqtt']['macs-topic'])

def on_mqtt_disconnect(client, userdata, rc):
    logger.info("MQTT disconnect occured. Result code %d", rc)
    if rc:
        logger.warning("Unexpected disconnection.")
    else:
        logger.info("Disconnection occured as responce to disconnect() call")

def on_mqtt_message(client, userdata, msg):
    payload = msg.payload.decode("utf-8")
    macs = payload.split()
    conn = conn_pool.getconn()
    cursor = conn.cursor();
    online_users = []
    for mac in macs:
        cursor.execute("SELECT name FROM users WHERE id = (SELECT user_id FROM macs WHERE mac = %s)", (mac,))
        user = cursor.fetchone()
        if user:
            online_users.append(user[0])
    unique_users = list(set(online_users))
    mqtt_client.publish(config['mqtt']['users-topic'], " ".join(unique_users))
    conn_pool.putconn(conn)
        
mqtt_client = mqtt.Client()
mqtt_client.on_connect = on_mqtt_connect
mqtt_client.on_message = on_mqtt_message
mqtt_client.username_pw_set(config['mqtt']['user'], config['mqtt']['password'])
mqtt_client.connect(config['mqtt']['host'], int(config['mqtt']['port']), 60)
mqtt_client.on_disconnect = on_mqtt_disconnect

mqtt_client.loop_forever()
