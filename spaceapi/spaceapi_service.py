#!/usr/bin/env python

import json
import ftplib
import io
import yaml
import paho.mqtt.client as mqtt
import logging

CONFIG_FILE = "config.yml"
try:
    with open(CONFIG_FILE, 'r') as ymlfile:
        config = yaml.load(ymlfile, Loader=yaml.BaseLoader)
except IOError as e:
    logging.error("Config file not found!")
    logging.error("Exception: %s" % str(e))
    sys.exit(1)

logging.basicConfig(filename=config['logging']['spaceapi'], level=logging.INFO)

DATA_FILE = config['data']
try:
  with open(DATA_FILE, "r") as read_file:
      data = json.load(read_file)
except IOError as e:
    logging.error("Data file not found!")
    logging.error("Exception: %s" % str(e))
    sys.exit(1)

def send_data(data):
    datastring = json.dumps(data)
    bio = io.BytesIO(datastring.encode())
    session = ftplib.FTP(config['ftp']['host'], config['ftp']['user'], config['ftp']['password'])
    session.cwd(config['website']['folder'])
    session.storbinary('STOR spaceapi.json', bio)
    session.quit()

def on_mqtt_connect(client, userdata, flags, rc):
    if rc == 0:
        logging.info("Connection successful")
    elif rc == 1:
        logging.warning("Connection refused - incorrect protocol version")
    elif rc == 2:
        logging.warning("Connection refused - invalid client identifier")
    elif rc == 3:
        logging.warning("Connection refused - server unavailable")
    elif rc == 4:
        logging.warning("Connection refused - bad username or password")
    elif rc == 5:
        logging.warning("Connection refused - not authorised")
    else:
        logging.warning("Unknown connection error: %d", rc)
    client.subscribe(config['mqtt']['users-topic'])

def on_mqtt_disconnect(client, userdata, rc):
    logging.info("MQTT disconnect occured. Result code %d", rc)
    if rc != 0:
        logging.warning("Unexpected disconnection.")
    else:
        logging.info("Disconnection occured as responce to disconnect() call")

def on_mqtt_message(client, userdata, message):
    visitors = message.payload.decode("utf-8")
    is_open = len(visitors) > 0
    data['state']['open'] = is_open
    send_data(data)

mqtt_client = mqtt.Client()
mqtt_client.on_connect = on_mqtt_connect
mqtt_client.on_message = on_mqtt_message
mqtt_client.username_pw_set(config['mqtt']['user'], config['mqtt']['password'])
mqtt_client.connect(config['mqtt']['host'], int(config['mqtt']['port']), 60)
mqtt_client.on_disconnect = on_mqtt_disconnect

mqtt_client.loop_forever()
