mosquitto_pub -h $MQTT_SERVER -p $MQTT_SERVER_PORT -u $MQTT_USER -P $MQTT_PASS -t $MQTT_TOPIC -m "$(ssh $ROUTER "/ip arp print" | egrep -o '([0-9a-fA-F]:?){12}' | awk -vORS=' ' '{ print $1 }' | sed 's/, $//')"


