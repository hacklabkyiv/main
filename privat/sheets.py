import pickle
import os.path
import json
import datetime
import csv

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1cV5rDm37yy2rybVG0SwnZ2HGQrm4clMqPlpb8MGzsG4'
SAMPLE_RANGE_NAME = 'Names!A2:A'

creds = None
# The file token.pickle stores the user's access and refresh tokens, and is
# created automatically when the authorization flow completes for the first
# time.
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
# If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server()
    # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)

service = build('sheets', 'v4', credentials=creds)
sheet = service.spreadsheets()

beginning = datetime.datetime.today()
for i in range(1):
  beginning = beginning.replace(day=1) - datetime.timedelta(days=1)
  beginning = beginning.replace(day=1)
  fname = 'data/' + str(beginning.month) + '_' + str(beginning.year) + '.csv'
  sname = beginning.strftime('%B') + ' ' + str(beginning.year)
  print(fname, sname)
  with open(fname) as infile:
    data = []
    for row in csv.reader(infile):
      row[2] = float(row[2])
      if row[1] == 'D':
        row[2] = -row[2]
      else:
        if row[4] != '29024866200031':
          row[3] = row[3] + ' -> ' + row[4] + ' ' + row[5]
      data.append(list(map(str, [row[0], row[2], row[3]])))

    request = sheet.values().append(
        spreadsheetId=SAMPLE_SPREADSHEET_ID,
        range=sname + '!A1:A1',
        valueInputOption='RAW',
        body={'values': data})
    request.execute()

#result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
#                            range=SAMPLE_RANGE_NAME).execute()
#values = result.get('values', [])
#transformed = [str(row[0]) for row in values]
#print(transformed)

#if not values:
#    print('No data found.')
#else:
#    print('Name:')
#    for row in values:
#        # Print columns A and E, which correspond to indices 0 and 4.
#        print('>', row[0])
