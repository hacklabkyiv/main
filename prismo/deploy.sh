#!/bin/sh

# Deployment target settings
SERVER="pi@192.168.1.115"
SERVER_PATH="/home/pi/prismo"

rsync -rav -e ssh --exclude='config.cfg' --exclude='*.git' --exclude="deploy.sh" . $SERVER:$SERVER_PATH/
