#!/usr/bin/python

import sqlite3 as sqlite
import sys

dbTable = 'visitors.db'
keyFile = 'keys.txt'


def add_door_user(name):
    lastkey = ''
    with open(keyFile) as f:
	lastkey = f.read()

    if len(lastkey) != 6:
        print 'ERROR: Problem with last key, expected 6 symbols, got %d in %s' % (len(lastkey), lastkey)
        return

    conn = sqlite.connect(dbTable)
    c = conn.cursor()

    query = "SELECT COUNT(*) FROM users WHERE key = ?"
    c.execute(query, (lastkey,))
    count = c.fetchone()[0]
    if count > 0:
        print 'ERROR: Problem with last key %s, already exists in the database' % (lastkey,)
	return

    query = "INSERT INTO users (name, key) VALUES (?, ?)"
    c.execute(query, (name, lastkey))
    rowid = c.lastrowid

    conn.commit()
    conn.close() 

    print 'Successfully inserted %s with id %d' % (name, rowid)


def change_tool_permission(username, toolname, permission):
   if permission not in [0, 1]:
	print 'ERROR: permission %d is not <0|1>' % (permission,)
	return
   if toolname not in ['door', 'cnc', 'lathe', 'bigcnc']:
	print 'ERROR: tool %s is not <door|cnc|lathe|bigcnc>' % (toolname,)
	return

   conn = sqlite.connect(dbTable)
   c = conn.cursor()
   query = 'UPDATE users SET %s = ? WHERE name = ?' % (toolname,)
   c.execute(query, (permission, username)) 
   conn.commit()
   conn.close() 
   print 'Successfully given %s access %d to %s' % (username, permission, toolname)


def view_users():
   conn = sqlite.connect(dbTable)
   c = conn.cursor()
   c.execute("SELECT * FROM users")
   
   print ', '.join(map(lambda x: x[0], c.description))
   for row in c.fetchall():
       print ', '.join(map(str, row))

   conn.commit()
   conn.close() 


def change_username(username, new_username):
   conn = sqlite.connect(dbTable)
   c = conn.cursor()
   query = 'UPDATE users SET name = ? WHERE name = ?'
   c.execute(query, (new_username, username))
   conn.commit()
   conn.close()
   print 'Successfully renamed %s to %s' % (username, new_username)


def print_info():
     print "Database is at %s.\n\n\
USE: \n\
     <-s> shows user list \n\
     <-ad> [name] add user without rights\n\
		IMPORTANT: key comes from %s\n\
     <-cu> [username] [new username] change username\n\
     <-ct> [username] <door|cnc|lathe|bigcnc> <0|1> change user rights\n" % (dbTable, keyFile)


if __name__ == '__main__': 
    if len(sys.argv) == 1:
	print_info()

    elif sys.argv[1] == '-s':
	view_users()
       
    elif sys.argv[1] == '-ct':
	change_tool_permission(sys.argv[2], sys.argv[3], int(sys.argv[4]))
       
    elif sys.argv[1] == '-ad':
	add_door_user(sys.argv[2])

    elif sys.argv[1] == '-cu':
	change_username(sys.argv[2], sys.argv[3]) 
       
    else: 
	print_info()
