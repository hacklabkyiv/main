# Tools daemon setup

1. Install python3 requirements: `pip3 install -r requirements.txt`

2. Script are running under `supervisor` manager. Read detailed here: http://supervisord.org/index.html

   **Basic info:** 

   *Config file is here*: `/etc/supervisor/supervisord.conf`, all new programs can be added there.

   *Watch logs here:* `sudo supervisorctl`, then `tail -f <name of process>`

3. Example of config file:

   ```yaml
   doorbot-token: xoxp-1234567890-12345689-your-token
   # Visitors database config
   db-config:
     server: <host with running database>
     port: <port>
     user: <username>
     password: <password>
   latest-key-file: keys.txt
   # Machines config
   cnc:
     server_name: 192.168.1.115
     server_port: 1234
     db_index: 4
     log_filename: logs/cnclog.txt
   bigcnc:
     server_name: 192.168.1.115
     server_port: 1236
     db_index: 6
     log_filename: logs/bigcnclog.txt
     # seconds until user is forced out
     keepalive: 120
   lathe:
     server_name: 192.168.1.115
     server_port: 1235
     db_index: 5
     log_filename: logs/lathelog.txt
   
   ```

   

# Doors daemon setup

Compared to device RFID reader, door has another model of reader: CP-Z2L from *Iron Logic*(datasheet here: https://www.ironlogic.ru/il.nsf/file/ru_cp-z-2l.pdf/$FILE/cp-z-2l.pdf)

Doors daemon running on the same way as daemon_tools, config example is:

```yaml
doorbot-token: xoxp-3343434-343434343-343434-yourtoken
db-config:
  server: <host ip>
  port: <port>
  user: <username>
  password: <password>

```

## Installing watchdog

1. Instructoions was taken here: https://raspberrypi.stackexchange.com/questions/68331/how-to-keep-watchdog-timer-running-during-reboot-shutdown
2. Removed swap file 
```
pi@raspberrypi:/ $ sudo dphys-swapfile swapoff
pi@raspberrypi:/ $ sudo dphys-swapfile uninstall
pi@raspberrypi:/ $ sudo update-rc.d dphys-swapfile remove
pi@raspberrypi:/ $ sudo apt purge dphys-swapfile
```
3. `/etc/watchdog.conf `
```
#ping                   = 172.31.14.1
#ping                   = 172.26.1.255
#interface              = eth0
#file                   = /var/log/messages
#change                 = 1407

# Uncomment to enable test. Setting one of these values to '0' disables it.
# These values will hopefully never reboot your machine during normal use
# (if your machine is really hung, the loadavg will go much higher than 25)
max-load-1              = 10
#max-load-5             = 18
#max-load-15            = 12

# Note that this is the number of pages!
# To get the real size, check how large the pagesize is on your machine.
#min-memory             = 1
#allocatable-memory     = 1

#repair-binary          = /usr/sbin/repair
#repair-timeout         = 60
#test-binary            =
#test-timeout           = 60

# The retry-timeout and repair limit are used to handle errors in a more robust
# manner. Errors must persist for longer than retry-timeout to action a repair
# or reboot, and if repair-maximum attempts are made without the test passing a
# reboot is initiated anyway.
#retry-timeout          = 60
#repair-maximum         = 1

watchdog-device = /dev/watchdog
watchdog-timeout = 14

# Defaults compiled into the binary
#temperature-sensor     =
#max-temperature        = 90

# Defaults compiled into the binary
#admin                  = root
interval                = 5
logtick                 = 60
log-dir         = /var/log/watchdog

# This greatly decreases the chance that watchdog won't be scheduled before
# your machine is really loaded
realtime                = yes
priority                = 1

# Check if rsyslogd is still running by enabling the following line
#pidfile                = /var/run/rsyslogd.pid
```

