#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
# Caution!
# Added shebang with -u option to enable autoflush of stdout!
import socket
import psycopg2 as psycopg
from psycopg2.extensions import AsIs
from time import localtime, strftime
from datetime import datetime, date, MINYEAR, MAXYEAR
from datetime import timedelta
import time
import slack
import sys
import yaml

try:
    from yaml import CLoader as Loader, CDumper
except ImportError:
    from yaml import Loader

if len(sys.argv) == 1:
   print("Tool name is not provided!")
   exit(1)

CONFIG_FILE = 'config.cfg'
cfg = yaml.load(open(CONFIG_FILE, 'r'), Loader=Loader)
DOORBOT_TOKEN = cfg['doorbot-token']

tool_name = sys.argv[1]
if not tool_name in cfg:
    print("Tool name unknown", tool_name)
    exit(2)

tool = cfg[tool_name]
server_name = tool['server_name']
server_port = int(tool['server_port'])
db_toolname = tool['db_toolname']      
log_filename = tool['log_filename']
latest_key_file = cfg['latest-key-file']
keepalive = tool['keepalive'] if 'keepalive' in tool else 0

slack_client = slack.WebClient(token=DOORBOT_TOKEN)

def parse_log(input_log, date_start=None, date_end=None, verbose=False):    
    """
    Input date example: 2016-09-18
    
    Usage example:
    >>> parse_log("mylog", "2016-10-10", "2016-12-10")
    """
    summary = dict()
    # Filling date boundaries
    if date_start:
        date_start = datetime.strptime(date_start, "%Y-%m-%d")
    else:
        date_start = datetime(MINYEAR, 1, 1, 0, 0, 0)
        
    if date_end:
        date_end = datetime.strptime(date_end, "%Y-%m-%d")
    else:
        date_end = datetime(MAXYEAR, 1, 1, 0, 0, 0)
    
    # Parse log file
    with open(input_log, 'r') as f:
        start_time = None
        start_owner = None
        start_stop_mode = None
        for line in f.readlines():
            line_words = line.split()        
            try:          
                datetime_string = ' '.join(line_words[1:3])
                date_object = datetime.strptime(datetime_string, "%Y-%m-%d %H:%M:%S")
                if line_words[3] == '1':
                    start_time = date_object
                    start_owner = line_words[0]
                elif (line_words[3] == '0') and (start_stop_mode == '1'):
                    if date_start < start_time < date_end:                        
                        # Flush data when '1' switches to '0'
                        consumed_time = (date_object-start_time)                        
                        try:
                            # Try to add if key exists
                            summary[start_owner] += consumed_time
                        except KeyError:
                            # Create new key if not exists
                            summary[start_owner] = consumed_time
                        if verbose != False:
                            print(datetime_string, "Name:", start_owner,
                                  "Consumed time: ", consumed_time)
                start_stop_mode = line_words[3]
            except ValueError:
                pass
    return summary

def get_monthtime_for_user(logfile, username):
    now = datetime.now()
    
    monthstart = now.strftime("%Y-%m-01")
    nowdate = now.strftime("%Y-%m-%d")
    result = parse_log(logfile, monthstart)
    
    return result[username]

def report_user_time(u):
    print("------- REPORT USER TIME -------------")
    used_time = get_monthtime_for_user(log_filename, u)
    max_time = timedelta(hours=12, minutes=0, seconds=0)
    print("Used time:", used_time)
    print("Max allowed time:", max_time)
    if used_time < max_time:
        try:
            message = 'You have used %s of %s time this month' % (used_time, tool_name)
            channel = '@%s' % u
            slack_client.chat_postMessage(channel=channel, text=message, as_user=True)
        except:
            message = '%s used %s of %s time this month'%(u, tool_name)
            channel = '#tools_time'
            slack_client.chat_postMessage(channel=channel, text=message, as_user=True)
    else:
        try:
            channel = '@%s' % u
            message = ':exploding_head: ACHTUNG ALARM! %s have used %s of %s time this month, STAHP! It is too much!' % (u, used_time, tool_name)
            slack_client.chat_postMessage(channel=channel, text=message, as_user=True)
            channel = '#tools_time'
            slack_client.chat_postMessage(channel=channel, text=message, as_user=True)
        except:
            message = ':exploding_head: ACHTUNG ALARM! %s have used %s of %s time this month, STAHP! It is too much!' % (u, used_time, tool_name)
            channel = '#tools_time'
            slack_client.chat_postMessage(channel=channel, text=message, as_user=True)


def get_user(key, toolname):
    print("Get user for key:", key)
    conn = psycopg.connect(user = cfg['db-config']['user'],
                                  password = cfg['db-config']['password'],
                                  host=cfg['db-config']['server'],
                                  port=cfg['db-config']['port'],
                                  database='visitors')
    c = conn.cursor()
    c.execute("SELECT %s, name  FROM users WHERE key=%s", (AsIs(toolname), key.decode('utf-8')))
    result = c.fetchone()  
    conn.close()
    print("Database query ok, result:", result)
    return result

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


# Bind the socket to the address given on the command line
server_address = (server_name, server_port)
sock.bind(server_address)
sock.settimeout(60) # 1 min
sock.listen(1)

state = 0
current_user = None
report = False
last_keepalive = time.time()

print("Tools daemon started for ", tool_name)
if keepalive:
    print("(keepalive enabled)")
while True:
    try:
        connection, client_address = sock.accept()
        print("Socket accepted, client address is:", client_address)
        try:
            while True:
                data = connection.recv(16)
                if data:          
                    last_keepalive = time.time()
                    if len(data) > 4:
                        # Data must be stripped in exactly this weird way...
                        u = get_user(data[4:10], db_toolname)
                        # Write last key to file
                        with open(latest_key_file, 'w') as f:
                            f.write(data[4:10].decode("utf-8"))
                        print("Latest key file was written")
                        if u != None:  			   
                            print("User data was found: ", u)
                            if u[0] == 1:
                                if state == 0:
                                    s = "%-20s %s 1\n"%(u[1], strftime("%Y-%m-%d %H:%M:%S", localtime())) 
                                    connection.sendall(b'1')
                                    state = 1
                                    current_user = u[1]
                                else: 
                                    s = "%-20s %s 0\n"%(u[1], strftime("%Y-%m-%d %H:%M:%S", localtime()))						
                                    connection.sendall(b'0')
                                    state = 0
                                    current_user = None
                                    report = True    
                            else:
                                s = "%-20s %s 0\n"%(u[1], strftime("%Y-%m-%d %H:%M:%S", localtime()))
                                connection.sendall(b'0')
                                state = 0;
                                current_user = None
                        else:
                            print("User data was not found! Access denied!")
                            s = "%-20s %s 0\n"%('unknown',strftime("%Y-%m-%d %H:%M:%S", localtime()))
                            connection.sendall(b'0')
                            state = 0
                            current_user = None
                        

                        print(s)	
                        with open(log_filename, "a") as f:
                            f.write(s)
                            f.close()
                            
                        if report:
                           report_user_time(u[1])
                           report = False
                else:
                    break
                
        except:
            pass
            
        finally:
            connection.close()
    except socket.timeout:
        pass # this is important only because we want regular keepalive checks below
        
    except:
        raise # we only care about socket timeouts

    if keepalive and state == 1 and time.time() - last_keepalive > keepalive:
        print("Force user out: ", current_user)
        s = "%-20s %s 0\n" % (current_user, strftime("%Y-%m-%d %H:%M:%S", localtime()))
        with open(log_filename, "a") as f:
            f.write(s)
            f.close()
        report_user_time(current_user)
        state = 0
        current_user = None
